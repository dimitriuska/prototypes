
/**
 * Module dependencies.
 */

var express = require('express')
   , http = require('http');
var app = module.exports = express.createServer();
var url = require('url');
var fs = require('fs');

// Serial Port 

var serialport = require("serialport"); 
var SerialPort = serialport.SerialPort; 
var serialPort = new SerialPort("/dev/ttyACM0", {
  baudrate: 115200,
  parser: serialport.parsers.readline("\n")
});


var server = app.listen(3000);

 // this tells socket.io to use our express server

var io = require('socket.io').listen(server);

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

mongoose.connect('mongodb://localhost:27017/measures');

var Measures = new Schema({
    StatusA:      { type: String },
    temperature: {
        A:  { type: Number },
        B:   { type: Number }
    },
    cicleA:      { type: String },
    ResistanceA:   { type: String }
});


var  Measures = mongoose.model('Measures', Measures);


// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});


// Socket Connection  - write 

io.on("connection", function(socket) {
     socket.on("tweetprocesson", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
          serialPort.open(function (error) {
                          if ( error ) {
                              console.log('failed to open: '+error);
                                } else {
                             serialPort.write("/process?params="+tweet+"\n\r")};
                          });
                });


     socket.on("tweetprocessoff", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
           serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/process?params="+tweet+"\n\r")};
                        });
     });
   
     socket.on("tweetTHAmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHAmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHBmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHB?params="+tweet+"\n\r")};
                        });
     });
     
     socket.on("tweetTHBmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHB?params="+tweet+"\n\r")};
                        });
     });


     socket.on("tweetresPWM", function (tweet) {
        // we received a tweet from the browser
      console.log("/resAPWM?params="+tweet+"\n\r");
      // find each person with a last name matching 'Ghost', selecting the `name` and `occupation` fields
      Measures.findOne({ 'cicleA': 'Vanzi' }, function (err, measure) {
           if (err) return handleError(err);
              console.log(measure) // Space Ghost is a talk show host.
        });
     });

    
});

// serial port - socket read 

serialPort.on("open", function () {
//  console.log('open');

  serialPort.on('data', function(data) {
        console.log(data);
        Measures.create({'temperature.A': 32 , 'cicleA': 'Vanzi' });
        io.sockets.emit('giornata', { 'some': data }); 
    });
});


// start server 

app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
