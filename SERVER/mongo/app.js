var	MongoClient	=	require('mongodb').MongoClient;

var	dbhost	=	'mongodb://localhost:27017/test',
				myCollection	=	'chapter2';

var	seedData	=	function(db,	callback)	{
    db.collection(myCollection).find({},	{},	{})
        .toArray(
            function(err,	docs)	{
               if	(docs.length	<=	0)	{
                     console.log('No	data.	Seeding...');

                     //	count	each	record	as	its	inserted
                     var	ihandler	=	function(err,	recs)	{
                            if	(err)	throw	err;
                            inserted++;
                     }

                     var	toinsert	=	2,
                            inserted	=	0;

                     //	perform	a	MongoDB	insert	for	each	record
                     db.collection(myCollection).insert({
                         'Title':	'Snow	Crash',
                         'Author':	'Neal	Stephenson'
                     },	ihandler);
                     db.collection(myCollection).insert({
                         'Title':	'Neuromancer',
                         'Author':	'William	Gibson'
                     },	ihandler);

                     //	wait	for	the	2	records	above	to	be	finished inserting	//

                     var	sync	=	setInterval(function(){
                            if(inserted	===	toinsert)	{
                                clearInterval(sync);
                                callback(db);	
                            }
                     },	50);
                     return;
                 }
                 callback(db);  
                 return;
            }
        );
 }                  


