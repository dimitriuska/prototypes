var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

mongoose.connect('mongodb://localhost:27017/meas');

var Measures = new Schema({
    timestamp: ObjectId ,
    StatusA:  String ,
    temperature: {
        A: Number },
        B: Number }
    },
    cicleA: String,
    ResistanceA: String 
});


var  measures = mongoose.model('Measures', Measures);

measures.create({'temperature.A': 32 , 'cicleA': 'Vanzi' });


// find each person with a last name matching 'Ghost', selecting the `name` and `occupation` fields
measures.findOne({ 'cicleA': 'Vanzi' }, function (err, measure) {
  if (err) return handleError(err);
  console.log(measure) // Space Ghost is a talk show host.
})
